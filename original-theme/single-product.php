<?php get_header(); ?>

<p>個別の商品ページ</p>

<!--このループで記事の内容を表示-->
<?php if (have_posts()): ?>
    <?php foreach ($posts as $post): ?>
        <?php the_title(); ?>
        <?php echo the_content(); ?>
    <?php endforeach; ?>
<?php endif; ?>

<?php get_footer(); ?>