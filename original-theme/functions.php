<?php
function my_enqueue_scripts() {
	wp_enqueue_script('jquery');
	wp_enqueue_script('index_js', get_template_directory_uri(). '/assets/js/index.js', array());
	wp_enqueue_style('normalize', get_template_directory_uri(). '/assets/css/normalize.css', array());
	wp_enqueue_style('my_style', get_template_directory_uri(). '/assets/css/style.css', array('normalize'));
}
add_action( 'wp_enqueue_scripts', 'my_enqueue_scripts' );

//　投稿一覧ページの設定
function post_has_archive($args, $post_type) {
	if ('post' == $post_type) {
		$args['rewrite'] = true;
		$args["label"] = false; /*「投稿」のラベル名 */
		$args['has_archive'] = 'archive'; /* アーカイブにつけるスラッグ名 */
	}
	return $args;
}
add_filter('register_post_type_args', 'post_has_archive', 10, 2);