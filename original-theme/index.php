<?php 
/*
フロントページの優先順位
front-page.php　＞　page-[slug].php　＞　page-[id].php　＞　page.php　＞　singular.php　＞　index.php
index.phpは最低限必要（？）だけど
優先順位が低いから
front-page.phpがあると、index.phpは読み込まれない
*/