<!--固定ページ全部共通になるから、このファイルは無くても良い-->

<?php get_header(); ?>

<p>固定ページ</p>

<!--各固定ページの内容を表示-->
<!--ここでは管理画面で作った固定ページの内容が表示される-->
<?php
if(have_posts()):
	while(have_posts()): the_post();
		the_content();
	endwhile;
endif;
?>

<?php get_footer(); ?>
