<?php get_header(); ?>

<p>アバウトページ</p>

<!--アバウトページの内容を表示-->
<!--ここでは管理画面で作った固定ページの内容が表示される-->
<?php
if(have_posts()):
	while(have_posts()): the_post();
		the_content();
	endwhile;
endif;
?>

<?php get_footer(); ?>