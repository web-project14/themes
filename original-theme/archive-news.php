<?php get_header(); ?>

<!--ニュース一覧の表示-->
    <?php if (have_posts()): ?>
        <?php foreach ($posts as $post): ?>
            <?php the_title(); ?>
            <a href="<?php the_permalink(); ?>"><p>記事の表示</p></a>
        <?php endforeach; ?>
    <?php endif; ?>
</main>

<?php get_footer(); ?>