<?php get_header(); ?>

<p>個別の投稿記事ページ</p>

<!--このループで記事の内容を表示-->
<?php if (have_posts()): ?>
    <?php foreach ($posts as $post): ?>
        <?php the_title(); ?>
        <?php echo the_content(); ?>
        <?php echo the_post_thumbnail(); ?>
        <?php echo the_time(); ?>
        <?php echo the_category(); ?>
        <?php echo the_tags(); ?>

        <!--
        the_title：投稿のタイトルを出力する
        the_content：投稿の本文を出力する
        the_post_thumbnail：投稿のアイキャッチ画像を出力する
        the_time：投稿の日時を出力する
        the_category：投稿が属するカテゴリーのページへのリンクをリストで出力する
        the_tags：投稿につけられたタグのページへのリンクをリストで出力する
        -->
    <?php endforeach; ?>
<?php endif; ?>

<?php get_footer(); ?>