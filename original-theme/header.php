<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1" />
    <meta name="description" content="">
    <title>サイトのタイトル</title>
<?php wp_head(); ?>
</head>
<body>
    <header>
        <nav>
            <ul class="Header__g-navi" id="page-link">
                <li><a href="<?php echo home_url(); ?>">HOME</a></li>
                <li><a href="<?php echo home_url(); ?>/archive">投稿記事</a></li>
                <li><a href="<?php echo home_url(); ?>/product">商品</a></li>
                <li><a href="<?php echo home_url(); ?>/news">ニュース</a></li>
                <li><a href="<?php echo home_url(); ?>/about">アバウト</a></li>
                <li><a href="<?php echo home_url(); ?>/privacy">プライバシーポリシー</a></li>
            </ul>
        </nav> 
    </header>

