# WordPress オリジナルテーマテンプレート

## 初期設定
1. wp-dockerで作成したWordPressから、thems以下を削除
2. wp-contentの階層へ移動
3. ```git clone https://gitlab.com/web-project14/themes.git```  
する。
4. WordPress管理画面で「外観」＞「テーマ」から「オリジナルテーマ名」を選ぶ

## editorの設定について
- VSCodeのプラグイン  
```editor config vscode```  
をインストール、有効化しておく
これで各ファイルのインデントが、original-theme内の.editorconfigの設定によって統一される

## saasのコンパイルについて
1. ```nvm use 16```  
nodeはバージョン16を使用する
2. ```cd original-theme```
で、テーマフォルダへ移動
3. ```npm install```  
でpackage.jsonからsassをインストールする
4. ```npm run sass```  
でsassのコンパイルを行う




